# angular-click-blur
Blur html elements after clicking them.

## Install
```sh
bower install --save git@git.ktlxv.com:root/angular-click-blur.git
```
## Add to angular

Include `angular-click-blur.js` in your HTML document somewhere after you have set
up AngularJS.

```html
<script src="angular-click-blur.js">
```

Make `angular-click-blur` a dependency in your AngularJS app.

```js
angular.module("myApp", ["angular-click-blur"]);
```

## Use
Add a `click-blur` attribute to any clickable element.