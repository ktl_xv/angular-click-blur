'use strict';

angular.module('angular-click-blur', [])
	.directive('clickBlur', function() {
	return {
		restrict: 'A',
		link: function (scope, element) {
			element.bind('click', function () {
				element.blur();
			});
		}
	};
});
